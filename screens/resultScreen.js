import React from 'react';
import { 
  StyleSheet, 
  View, 
  Text,
  Image
} from 'react-native';

import { annotateText } from '../api/googleNaturalLanguage';

export default class ResultScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Results"
  });

  constructor(props) {
    super(props);

    const { navigation } = props;
    const inputText = navigation.getParam('inputText', 'ERROR');

    this.state = {
      magnitude: '',
      score: '',
      isTextAnalyzed: false,
      inputText
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const inputText = navigation.getParam('inputText', 'ERROR');
    if (inputText) {
      this.handleAnalysis(inputText);
    }
  }

  handleAnalysis = async (text) => {
    try {
      const result = await annotateText(text);
      console.log('Analysis: ', result);
      const { 
        documentSentiment: {
          magnitude, score
        }
      } = result;
      this.setState({ 
        isTextAnalyzed: true,
        magnitude, 
        score });
    } catch (e) {
      console.log(e);
    }
  }

  renderLoading() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          Analyzing input...
        </Text>
      </View>
    );
  }

  renderResult() {
    const { inputText, magnitude, score } = this.state;
    const resultText = score < 0
      ? "Your text contains aggressive words"
      : "You are good to go!"
    const imageSrc = score < 0
      ? require('../assets/noGood.png')
      : require('../assets/ok.png')
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {`Your comment: ${inputText}\n\n`}
          {`Magnitude: ${magnitude}\n\n`}
          {`Score: ${score}\n\n`}
          {resultText}
        </Text>
        <Image
          source={imageSrc}
        />
      </View>
    );
  }

  render() {
    const { isTextAnalyzed } = this.state;

    if (isTextAnalyzed) {
      return this.renderResult();
    } else {
      return this.renderLoading();
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 25,
    marginBottom: 25,
    width: '80%'
  }
});
