import React from 'react';
import { 
  StyleSheet, 
  View, 
  Text 
} from 'react-native';

export default class AboutScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "About"
  });

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {"HOST"}
        </Text>
        <Text style={styles.text}>
          {"An attempt to avoid cyber bullying of a user through Machine Learning"}
        </Text>
        <Text style={styles.text}>
          {"According to several studies, cyber bullying has increased in these days of social media. Cyber bullying can happen due to a variety of reasons such as peer pressure, want of revenge, basic lack of empathy etc.,. It is extremely important to protect and shield the victims of cyber bullying as they run the risk of being psychologically damaged which can potentially affect their future."}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  text: {
    width: '80%',
    fontSize: 25,
    margin: 20
  },
});
