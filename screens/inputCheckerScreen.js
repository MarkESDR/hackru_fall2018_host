import React from 'react';
import { 
  StyleSheet, 
  TextInput, 
  View, 
  Button, 
  Text,
  ImageBackground
} from 'react-native';

import { 
  ABOUT_ROUTE,
  RESULT_ROUTE
} from '../constants/routes'

export default class InputCheckerScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "HOST",
    headerRight: (
      <Button 
        title="Info"
        onPress={() => {navigation.navigate(ABOUT_ROUTE)}}
      />
    )
  });

  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  render() {
    const { navigation } = this.props;
    return (
      <ImageBackground
        source={require('../assets/bg.jpg')}
        style={styles.container}
      >
        <View
          style={styles.topMargin}
        /> 
        <TextInput 
          style={styles.textbox}
          placeholder="Type message"
          onChangeText={(text) => this.setState({text})}
        />
        <Button
          title="Analyze"
          onPress={() => {navigation.navigate(RESULT_ROUTE, {
            inputText: this.state.text
          })}}
        />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    height: '100%'
  },
  topMargin: {
    height: 20
  },
  textbox: {
    width: '80%',
    height: '40%',
    backgroundColor: 'white',
    borderRadius: 20,
    marginBottom: 20,
    padding: 20,
    fontSize: 20
  },
  text: {
    fontSize: 42
  },
  background: {
  }
});
