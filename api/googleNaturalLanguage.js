import { GOOGLE_API_KEY } from '../config/googleApi.json';

const ANALYZE_SENTIMENT_URL = 'https://language.googleapis.com/v1/documents:analyzeSentiment';
const ANNOTATE_TEXT_URL = 'https://language.googleapis.com/v1beta2/documents:annotateText';

function createDocumentForText(text) {
  return {
    type: 'PLAIN_TEXT',
    content: text
  };
}

export async function analyzeText(text) {
  const url = `${ANALYZE_SENTIMENT_URL}?key=${GOOGLE_API_KEY}`;
  const body = {
    document: createDocumentForText(text),
    encodingType: 'UTF16'
  }
  console.log("Sending request...")
  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
    .then((response) => {
      return response.json();
    })
    .catch((error) => console.log(error));
}

export async function annotateText(text) {
  // Google api requires text to have a certain amount of words
  // to analyze entities
  const isLongEnough = text.split(' ').length >= 20;

  const url = `${ANNOTATE_TEXT_URL}?key=${GOOGLE_API_KEY}`;
  const body = {
    document: createDocumentForText(text),
    encodingType: 'UTF16',
    features: {
      extractSyntax: isLongEnough,
      extractEntities: isLongEnough,
      extractDocumentSentiment: true,
      extractEntitySentiment: isLongEnough,
      classifyText: isLongEnough,
    }
  }
  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
    .then((response) => {
      return response.json();
    })
    .catch((error) => console.log(error));
}
