import React from 'react';
import { createStackNavigator } from 'react-navigation';

import InputCheckerScreen from './screens/inputCheckerScreen';
import AboutScreen from './screens/aboutScreen';
import ResultScreen from './screens/resultScreen';

import { 
  INPUT_CHECKER_ROUTE,
  ABOUT_ROUTE,
  RESULT_ROUTE
} from './constants/routes'

export default createStackNavigator({
  [INPUT_CHECKER_ROUTE]: {
    screen: InputCheckerScreen
  },
  [RESULT_ROUTE]: {
    screen: ResultScreen
  },
  [ABOUT_ROUTE]: {
    screen: AboutScreen
  }
}, {
  initialRouteName: INPUT_CHECKER_ROUTE
});